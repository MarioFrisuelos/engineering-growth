# Engineering Director

Role also known as development manager, is responsible for the consistent delivery, career growth and level of happiness of the team.

| Level | Seniority | Position |
| :---: | :---: | :---: |
| 5 | Senior | [ED5 - Engineering Director 5](#ed5---engineering-director-5) |


## ED5 - Engineering Director

### Technical skills 
- Ensures their organization has appropriately high technical competence and strives for excellence
- Researches new technologies to stay abreast of industry trends and standards. 
- Capable of jumping in to help debug and triage critical systems as needed. 
- Contributes to architecture by asking the right questions to ensure architecture matches business needs for the area

### Delivery 
- Develops and deploys new strategies for building a high velocity, high performance development organization in line with our emerging customer needs. 
- Supports technical innovation and leads the creation, continual refinement, and active enforcement of our development standards in order to ensure that our technology can be leveraged as a sustainable competitive advantage.
- Participates in staff recruitment, performance assessments, training, and career development. Responsible for all headcount planning and personnel evolution for multiple areas of the engineering organization.
- As necessary, the director manages vendor and external relationships for their organization, and participates in the budgeting process

### Impact 
- Provides leadership to software development managers, addresses technical, resource and personnel issues. 
- Proactively nurtures the talent of the senior staff in their areas. 
- Builds and supports high functioning, motivated teams. 
- Creates an organisation that understands how to balance technical debt vs business goals.

### Communication and leadership 
- Ensures that every member of the team understands the business goals for the quarter and has bought in to these goals
- Identifies areas for process evolution or clarification, gathers the stakeholders and creates and communicates the strategy for resolving these issues
- Clearly articulates the needs from a personnel and cultural standpoint that will move the engineering organisation to the next level

# Other Pages

* [**Introduction**](README.md)
* [**Developer**](engineer.md)
* [**Tech Lead**](tech-lead.md)