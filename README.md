# Introduction

This framework allows software engineering managers to have meaningful conversations with their direct reports around the expectations of each position and how to plan for the next level in the growth of their career at Internet Fusion Group.


# Axes

* **Technical skills**: knowledge of our technology stack and tools
* **Delivery**: the ability to get stuff done
* **Impact**: the impact you have on the organisation
* **Communication and leadership**: the ability to communicate clearly and provide leadership within the organisation


# Levels

## Engineer 1

Displays solid understanding of core CS fundamental concepts. Focused on growing as an engineer, learning the team’s tools and current processes, and developing productivity skills, as well as learning about the best-practices of software engineering such as testing, source control, and agile planning.

Capable of taking well-defined tactical sub-tasks from a larger project and completing these tasks in a reasonable time frame.

Focused on learning about a specific component or product sub-area and mastering that component. 

Communicates status to their manager and team, and strives to learn the Internet Fusion Group values and exhibit these values in their daily work.


## Engineer 2

An engineer two will enter this level capable of taking well-defined tasks and completing them in a way that is considered by the team to be high-quality with supervision from more senior team members. The progress through this level is focused on taking tasks of increasing complexity, scope and importance and completing them with very high quality with a lessening need for manager/tech lead oversight. 

This level is the bread-and-butter level of engineering growth. Engineers at this level should be focused on becoming great engineers, learning how to set high quality bars for their work without sacrificing productivity. All engineers at and above this level should religiously follow stated best practices for the team without excessive handholding. Engineers at this level will continue to make mistakes, but should be improving the speed at which they learn from these mistakes. By the time an engineer 2 is ready to be promoted s/he will have focused on some technology as their expertise and become capable of mentoring interns and new engineers in these areas. They will start to participate more in the technical design process, often with guidance from senior engineers.

Engineers at this level are assumed to be constantly making steady progress on tasks that are assigned to them and know when to ask for help when they are blocked. They can own their independent small-to-medium features all the way through from technical design to launch. They are capable of prioritising the work in front of them and able to make forward progress, avoiding the temptation to focus on unimportant details or excessive bikeshedding.

The impact at this level is focused on task completion and depth in a small area of the code base. Engineers at this level should be capable supporting release that include their area of expertise as well as provide technical support for simple incidents in areas that they are not always familiar with. 

They communicate well and are capable of delivering feedback to peers and their manager. When given a task with unclear requirements they know how to ask for clarification, and ensure that all assumptions are vetted before work starts to reduce the need for re-work. They understand how their work fits into the larger picture for their team, and use this to identify conflicting requirements to their tech lead and product manager. An important focus of this level is developing empathy for the users of their software, whether they be internal employees, customers, or other developers on the team. A team member at this level is seeking out the context they need to understand the why of a particular feature and nurturing this empathy via that understanding.


## Senior Engineer

The senior engineer should be seen as a rock-solid engineer who is an expert within their specific domain. The senior engineer is capable of owning technical design for projects of moderate complexity, and understands the tradeoffs in creating good software in their area. They hold a depth of knowledge in systems that enables them to debug those systems effectively without flailing. In addition to writing consistently high-quality code they are aware of industry best practices and trends, and have acquired at least one major skill outside of programming such as monitoring, performance optimisation, documentation, integration testing, visual design.

The senior engineer gets a lot done. They are responsible for complex tasks and complete them despite roadblocks, grabbing others for help or insight as necessary. The senior engineer requires very little oversight beyond high-level direction; they can take a complex user story, break it down into sub-tasks, and complete their sub-tasks with relative ease. The senior engineer shows initiative beyond knocking tasks off a list; they are able to identify and suggest areas of future work for themselves or their teams. They seek evidence to support their ideas and start to build cases for these ideas. They deliver products to test that they believe are well-baked and bug-free.

The senior engineer has end-to-end responsibility for projects of increasing complexity that encompass more than their own development. They contribute to the common code bases and standards for the team. They understand the business that their code supports, and possess empathy for the users of their software; they use this understanding to influence their task prioritisation. They assist QA in identifying and validating test cases and can identify regression risks in their features. In general, they can identify risks in code, features, and design, and communicate these to the appropriate parties.

The senior engineer is known outside of their core team as a technology leader. They participate extensively in code reviews, and mentor others via code reviews and pairing, as well as frequently presenting at Drinks and Demos and team meetings. They work effectively with non-tech members of the company. They are able to identify problems with requirements and help their team course-correct around these issues. 


## Tech Lead

The tech lead exhibits leadership and influence well beyond themselves. This leadership comes in the form of team leadership, exceptionally strong individual contribution, or something between these two extremes (extensive mentoring of junior team members while maintaining deep individual contribution, for example). Given a nebulous project, a tech lead will appropriately scope it, find a solution, implement and launch that solution.

The tech lead is viewed as the go-to expert in some significant area of the code base, and not just because they are the only person who has ever worked in that code base. They are involved in setting the standards for the entire organisation and providing technical advice and decision-making that affects not only their group but other teams or the company at large. They research and propose new technologies, and have a broad understanding of the entire architecture, as well as very detailed understanding of their area. They may not write as much code day to day but they still deliver features and have learned how to balance leadership and individual contribution.

The tech lead helps large groups of engineers deliver complex projects. They are known for drama-free launches, and own the technical testing and performance planning side of these projects. The tech lead knows how to do project management. They take long projects or complex groups of user stories and break this work down into milestones to avoid large monolithic deliverables. They strive to deliver software on-time and improve the accuracy of their team’s estimations. 

Beyond the day-to-day, the tech lead is starting to have a bigger impact beyond themselves and their immediate projects. They identify big issues and opportunities in the technology and organisation, and work across teams to create solutions to these issues. They proactively identify and clean up technical debt before it turns into a long-term problem, and encourage and enable their team to do the same. They are setting direction in some major part of the technology for their pillar and have a major role in the pillar or team’s decision-making process. 

As a leader, the tech lead contributes widely to making others better via code reviews, mentoring, and training. They will sit on architecture review boards as appropriate and may be asked to provide feedback on projects outside their area. They understand the tradeoffs between technical, analytical and product goals, and strive to create solutions that satisfy all of these goals. They know how to not only identify technical problems and create solutions, but are also able to get cross-team buy-in for their solutions and manage projects to make these solutions come to life. 


## Engineering Director

The engineering director is responsible for the technology team. 

The engineering director is not generally expected to write code on a day-to-day basis. However, the engineering director is responsible for their organisation’s overall technical competence, guiding and growing that competence in the whole team as necessary via training and hiring. They should have a strong technical background and spend some of their time researching new technologies and staying abreast of trends in the tech industry. They will be expected to help debug and triage critical systems, and should understand the systems they oversee well enough to perform code reviews and help research problems as needed. They should contribute to the architecture and design efforts primarily by serving as the technically-savvy voice that asks business and product questions of the engineers on their teams, ensuring that the code we are writing matches the product and business needs and can scale appropriately as those needs grow.

The engineering director is primarily concerned with ensuring smooth execution of complex deliverables. To that end, they focus on ensuring that we continually evaluate and refine our development/infrastructure standards and processes to create technology that will deliver sustained value to the business. They are responsible for creating high performance, high velocity organisations, measuring and iterating on processes as we grow and evolve as a business. They are the leaders for recruiting, headcount management and planning, career growth and training for the organisation. As necessary, directors will manage vendor relationships and participate in the budgeting process.

The impact of an engineering director should reach across multiple areas of the technology organisation. They are responsible for creating and growing the next generation of leadership and management talent in the organisation, helping that talent learn how to balance technical and people leadership and management. They are obsessed with creating high-functioning, engaged and motivated organisations, and they are expected to own retention goals in their organisation. Additionally engineering directors are responsible for strategically balancing immediate and long-term product/business focused work with technical debt and strategic technical development. 

Directors are strong leaders, and set the example for cross-functional collaboration both between technology and other areas of the company, and across divisions of technology. The goal of this collaboration is to create both a strategic and tactical tech roadmap that tackles both business needs, efficiencies and revenue as well as fundamental technology innovation. The director is a very strong communicator and can simplify technical concepts in a way to explain them to non-technical partners, and take business direction and explain it to the technology team in a way that inspires and guides them. Directors of engineering help to create a positive public presence for Internet Fusion Group tech and are capable of selling the company and their area to potential candidates. 

Due to their breadth of exposure to both technology and the business drivers, Directors are responsible for guiding the goal setting process for all of the teams in their organisation, helping these teams articulate goals that support both business initiatives and technology and organisational quality.



* [**Engineer**](engineer.md)
* [**Tech Lead**](tech-lead.md)
* [**Engineering Director**](engineering-director.md)