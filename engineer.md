# Engineer

Role also known as programmer or software engineer, requires a deep level of technical expertise.

| Level | Seniority | Position |
| :---: | :---: | :---: |
| 1 | Junior | [E1 - Engineer 1](#e1---engineer-1) |
| 2 | Midweight | [E2 - Engineer 2](#e2---engineer-2) |
| 3 | Senior | [E3 - Engineer 3](#e3---engineer-3) |


## E1 - Engineer 1

### Technical skills
- Broad knowledge of core CS concepts
- Focus on growing as an engineer, learning existing tools, resources and processes

### Delivery 
- Develops their productivity skills by learning source control, editors, the build system, and other tools as well as testing best practices
- Capable of taking well-defined sub-tasks and completing these tasks

### Impact 
- Developing knowledge of a single component of our architecture

### Communication and leadership 
- Effective in communicating status to the team, 
- Exhibits GSL's core values, focuses on understanding and living these values
- Accepts feedback graciously and learns from everything they do



## E2 - Engineer 2

### Technical skills
- Writes correct and clean code with guidance
- Consistently follows stated best practices
- Participates in technical design of features with guidance
- Rarely makes the same mistake twice
- Begins to focus on attaining expertise in one or more areas (eg, OpenDialog, conversation design, NLU etc). 
- Learns quickly and makes steady progress without the need for constant significant feedback from more senior engineers.

### Delivery 
- Makes steady progress on tasks; knows when to ask for help in order to get themselves unblocked
- Capable of taking well-defined sub-tasks and completing these tasks
- Able to own small-to-medium features from technical design through completion
- Capable of prioritizing tasks; avoids getting caught up in unimportant details and endless "bikeshedding"

### Impact 
- Self-sufficient in at least one large area of the codebase (OpenDialog core, OpenDialog webchat) with a high-level understanding of other components
- Capable of providing technicl support for their area including systems/components that they are not familiar with


### Communication and leadership 
- Gives timely, helpful feedback to peers and managers
- Communicates assumptions and gets clarification on tasks up front to minimize the need for rework
- Solicits feedback from others and is eager to find ways to improve
- Understands how their work fits in to the larger project and identifies problems with requirements

## E3 - Engineer 3

### Technical skills
- Understands and makes well-reasoned design decisions and tradeoffs in their area; able to work in other areas of the codebase with guidance. 
- Doesn't flail or panic while debugging.
- Demonstrates knowledge of industry trends, our infrastructure and our build system, including kubernetes, circleci, and git

### Delivery 
- Takes the initiative to fix issues before being assigned them. Seeks empirical evidence through proof of concepts, tests and external research
- Delivers complex products to QA that they believe are well-baked and bug-free

### Impact 
- Partners with design and UX and drive requirements that take into account all parties' needs
- Possesses empathy with the user of the software they are producing and uses that empathy to guide decision-making  
- Identifies problems/risks of own work and others.

### Communication and leadership 
- Communicates effectively cross functions
- Is able to work well with Design, UX etc, as necessary
- Proactively identifies problems with requirements (lack of clarity, inconsistencies, technical limitations) for their own work and adjacent work, and communicates these issues early to help course-correct

# Other Pages

* [**Introduction**](README.md)
* [**Tech Lead**](tech-lead.md)
* [**Engineering Director**](engineering-director.md)