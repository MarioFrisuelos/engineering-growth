# Tech Lead

Role also known as development lead, is the owner of the system and requires a unique balance between hands-on development, architecture knowledge and production support.

| Level | Seniority | Position |
| :---: | :---: | :---: |
| 4 | Senior | [TL4 - Tech Lead 4](#tl4---tech-lead-4) |

## TL4 - Tech Lead 4

### Technical skills 
- Go-to expert in one area of the codebase; understands the broad architecture of the entire system
- Provides technical advice and weighs in on technical decisions that impact other teams or the company at large. Researches and proposes new technologies.

### Delivery 
- Scopes and stages work into well-defined milestones to avoid a monolithic deliverable. 
- Regularly delivers software on-time and is constantly working to make accurate estimates and deliver on those estimates
- Known for drama-free launches
- Owns the technical testing and performance plan for their projects


### Impact 
- Takes initiative to identify and solve important problems, coordinating with others on cross-cutting technical issues
- Sets direction at the project/service level and consistently influences decision-making at the Pillar level
- Idenitifies and proactively tackles technical debt before it grows into debt that requires significant up-front work to resolve

### Communication and leadership 
- Makes others better through code reviews, thorough documentation, technical guidance, and mentoring or serving as a Tech Lead on a project
- Understands the tradeoffs between technical, analytical and product needs and leads to solutions that take all of these needs into account
- Identifies and proposes strategies around technical problems affecting their team, communicates standards and gets buy-in on solutions


# Other Pages

* [**Introduction**](README.md)
* [**Engineer**](engineer.md)
* [**Engineering Manager**](engineering-manager.md)